package com.zpc.order.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Item implements Serializable {
    private Long id;

    private String title;

    private String pic;

    private String desc;

    private Long price;

    public Item(){}

    public Item(long id, String title, String pic, String desc, Long price) {
        this.id=id;
        this.title=title;
        this.pic=pic;
        this.desc=desc;
        this.price=price;
    }
}
